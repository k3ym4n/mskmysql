CREATE DATABASE IF NOT EXISTS discotek;
use discotek;

CREATE TABLE IF NOT EXISTS grupo(
id int unsigned auto_increment primary key,
nombre VARCHAR (100),
genero VARCHAR (100),
fecha_inicio DATE,
fecha_fin DATE,
nacionalidad VARCHAR (125)
);

CREATE TABLE IF NOT EXISTS disco(
id int unsigned auto_increment primary key,
titulo VARCHAR (200),
duracion FLOAT  (5) unsigned,
productora VARCHAR (100),
num_canciones INT not null,
precio FLOAT  (6) unsigned,
nombre_grupo VARCHAR (100),

id_grupo int unsigned,
index (id_grupo),
foreign key (id_grupo)
references grupo (id) on delete cascade on update cascade
);

CREATE TABLE IF NOT EXISTS cancion(
id int unsigned auto_increment primary key,
titulo VARCHAR (200),
duracion FLOAT  (5) unsigned,
track INT unsigned,
precio FLOAT (6) unsigned,
genero VARCHAR (100),
titulo_disco VARCHAR (200),

id_disco int unsigned,
index(id_disco),
foreign key (id_disco)
references disco (id) on delete cascade on update cascade
);

CREATE TABLE IF NOT EXISTS usuario(
id int unsigned auto_increment primary key,
usuario VARCHAR (100) unique,
contrasena VARCHAR (25) unique
);