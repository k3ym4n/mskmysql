package org.dvd.Discotek.Interface;

import com.toedter.calendar.JDateChooser;
import sun.rmi.runtime.Log;

import javax.swing.*;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class Ventana {
    public JTabbedPane Panelprincipal;
    public JPanel TODO;
    public JTextField tfnombregrupo;
    public JTextField tfgenerogrupo;
    public JTextField tfnacionalidadgrupo;
    public JTextField tftitulodisco;
    public JTextField floatduraciondisco;
    public JTextField tfproductoradisco;
    public JTextField intnumcancionesdisco;
    public JTextField floatpreciodisco;
    public JTextField tftitulocancion;
    public JTextField floatduracioncancion;
    public JTextField inttrackcancion;
    public JTextField floatpreciocancion;
    public JTextField tfgenerocancion;
    public JTable tablagrupos;
    public JTable tabladiscos;
    public JTable tablacanciones;
    public JDateChooser datefiniciogrupo;
    public JDateChooser dateffingrupo;
    public JComboBox cbdiscosgrupo;
    public JTextField tfbuscarGrupo;
    public JTextField tfbuscarDisco;
    public Botonera botonera;
    public JComboBox cbcancionesgrupo;
    public JComboBox cbcancionesdisco;
    public JTextField tfbuscarcancion;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(TODO);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }
}
