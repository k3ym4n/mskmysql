package org.dvd.Discotek.Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class Configuracion extends JDialog implements ActionListener{
    private JPanel panelConfi;
    private JButton btconfirmar;
    private JTextField tfconfiusuario;
    private JTextField tfconficontrasena;
    private JTextField tfconfiservidor;
    private JTextField tfconfipuerto;
    private JTextField tfconfiDB;
    private JButton btcancelar;



    public Configuracion(){
        getContentPane().add(panelConfi);
        setLocationRelativeTo(null);
        pack();

        btconfirmar.addActionListener(this);
        btcancelar.addActionListener(this);
        cargarConfiguracion();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==btconfirmar){
            aceptarConfiguracion();
        }else if(e.getSource()==btcancelar){
            cancelar();
        }
    }

    private void cargarConfiguracion(){
        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream("Configuracion.properties"));
            tfconfiusuario.setText(prop.getProperty("usuario"));
            tfconficontrasena.setText(prop.getProperty("contrasena"));
            tfconfiservidor.setText(prop.getProperty("servidor"));
            tfconfipuerto.setText(prop.getProperty("puerto"));
            tfconfiDB.setText(prop.getProperty("base de datos"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void aceptarConfiguracion() {

        Properties prop = new Properties();

        prop.setProperty("usuario",tfconfiusuario.getText());
        prop.setProperty("contrasena",tfconficontrasena.getText());
        prop.setProperty("servidor",tfconfiservidor.getText());
        prop.setProperty("puerto",tfconfipuerto.getText());
        prop.setProperty("base de datos", tfconfiDB.getText());

        try {
            prop.store(new FileOutputStream("Configuracion.properties"), "Configuracion de la conexion");
            JOptionPane.showMessageDialog(null, "Configuracion guardada correctamente");
            setVisible(false);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Configuracion no guardada");
            return;
        }
    }

    private void cancelar(){
        setVisible(false);
    }

    public void mostrarConfiguracion(){
        setLocationRelativeTo(null);
        cargarConfiguracion();
        setVisible(true);
    }

    public JTextField getTfconfiusuario() {
        return tfconfiusuario;
    }

    public void setTfconfiusuario(JTextField tfconfiusuario) {
        this.tfconfiusuario = tfconfiusuario;
    }

    public JTextField getTfconficontrasena() {
        return tfconficontrasena;
    }

    public void setTfconficontrasena(JTextField tfconficontrasena) {
        this.tfconficontrasena = tfconficontrasena;
    }

    public JTextField getTfconfiservidor() {
        return tfconfiservidor;
    }

    public void setTfconfiservidor(JTextField tfconfiservidor) {
        this.tfconfiservidor = tfconfiservidor;
    }

    public JTextField getTfconfipuerto() {
        return tfconfipuerto;
    }

    public void setTfconfipuerto(JTextField tfconfipuerto) {
        this.tfconfipuerto = tfconfipuerto;
    }

    public JTextField getTfconfiDB() {
        return tfconfiDB;
    }

    public void setTfconfiDB(JTextField tfconfiDB) {
        this.tfconfiDB = tfconfiDB;
    }
}
