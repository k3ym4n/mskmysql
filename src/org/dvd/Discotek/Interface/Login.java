package org.dvd.Discotek.Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class Login extends JDialog implements ActionListener,KeyListener{
    private JPanel panelLogin;
    private JButton btok;
    private JButton btcancelar;
    private JTextField tfusuario;
    private JTextField tfcontrasena;

    private String usuario;
    private String contrasena;



    public Login(){
        super();
        getContentPane().add(panelLogin);
        setLocationRelativeTo(null);
        pack();
        setModal(true);

        btok.addActionListener(this);
        btcancelar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==btok){
            OK();
        }else if(e.getSource()==btcancelar){
            cancelar();
        }

    }

    private void OK(){
        if(tfusuario.getText().equals("") /*|| tfcontrasena.getText().equals("")*/){
            JOptionPane.showMessageDialog(null,"Es obligatorio introducir los dos campos correctamente (usuario y contarseņa)");
            return;
        }else{
            usuario = tfusuario.getText();
            contrasena = String.valueOf(tfcontrasena.getText());
            setVisible(false);

        }
    }

    private void cancelar(){
        System.exit(0);
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }


}
