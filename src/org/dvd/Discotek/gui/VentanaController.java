package org.dvd.Discotek.gui;

import org.dvd.Discotek.Interface.Botonera;
import org.dvd.Discotek.Interface.Configuracion;
import org.dvd.Discotek.Interface.Ventana;
import org.dvd.Discotek.base.Cancion;
import org.dvd.Discotek.base.Disco;
import org.dvd.Discotek.base.Grupo;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class VentanaController implements ActionListener , KeyListener , MouseListener{

    private Ventana view;
    private VentanaModel model;
    private Botonera botonera;

    private Configuracion configuracion;

    private DefaultTableModel modelotablaGrupos;
    private DefaultTableModel modelotablaDiscos;
    private DefaultTableModel modelotablaCanciones;

    private ArrayList<Grupo> listaGrupo;
    private ArrayList<Disco> listaDisco;

    private int idGrupo;
    private int idDisco;
    private int idCancion;


    private boolean modificar;


    public VentanaController(Ventana view, VentanaModel model, Botonera botonera) {
        this.view = view;
        this.model = model;
        this.botonera = botonera;

        listaGrupo = new ArrayList<>();
        listaDisco = new ArrayList<>();

        model.Login();

        if (model.isAdmin()) {
            botonera.btconfi.setEnabled(true);
        }else{
            botonera.btconfi.setEnabled(false);
        }

        view.Panelprincipal.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int pestana = view.Panelprincipal.getSelectedIndex();

                switch (pestana) {
                    case 0:
                        edicionCampos(false);
                        modeloTablas();
                        listarTablas();
                        combosRefrescar();
                        limpiarTF();
                        botonera.btnuevo.setEnabled(true);
                        botonera.btguardar.setEnabled(false);
                        break;
                    case 1:
                        edicionCampos(false);
                        modeloTablas();
                        listarTablas();
                        combosRefrescar();
                        limpiarTF();
                        botonera.btnuevo.setEnabled(true);
                        botonera.btguardar.setEnabled(false);
                        break;
                    case 2:
                        edicionCampos(false);
                        modeloTablas();
                        listarTablas();
                        combosRefrescar();
                        limpiarTF();
                        botonera.btnuevo.setEnabled(true);
                        botonera.btguardar.setEnabled(false);
                        break;
                    default:
                        break;
                }
            }
        });

        view.tablagrupos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int rowG = view.tablagrupos.rowAtPoint(e.getPoint());

                view.tfnombregrupo.setText(view.tablagrupos.getValueAt(rowG, 0).toString());
                view.tfgenerogrupo.setText(view.tablagrupos.getValueAt(rowG, 1).toString());
                view.datefiniciogrupo.setDate(Date.valueOf(view.tablagrupos.getValueAt(rowG, 2).toString()));
                view.dateffingrupo.setDate(Date.valueOf(view.tablagrupos.getValueAt(rowG, 3).toString()));
                view.tfnacionalidadgrupo.setText(view.tablagrupos.getValueAt(rowG, 4).toString());

                botonera.bteliminar.setEnabled(true);
                botonera.btmodificar.setEnabled(true);
            }
        });
        view.tabladiscos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int rowD = view.tabladiscos.rowAtPoint(e.getPoint());

                view.tftitulodisco.setText(view.tabladiscos.getValueAt(rowD, 0).toString());
                view.floatduraciondisco.setText(view.tabladiscos.getValueAt(rowD, 1).toString());
                view.tfproductoradisco.setText(view.tabladiscos.getValueAt(rowD, 2).toString());
                view.intnumcancionesdisco.setText(view.tabladiscos.getValueAt(rowD, 3).toString());
                view.floatpreciodisco.setText(view.tabladiscos.getValueAt(rowD, 4).toString());
                view.cbdiscosgrupo.setSelectedItem(view.tabladiscos.getValueAt(rowD, 5).toString());

                //variable para que el combo sea 0 en vez de -1

                botonera.bteliminar.setEnabled(true);
                botonera.btmodificar.setEnabled(true);
            }
        });
        view.tablacanciones.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int rowC = view.tablacanciones.rowAtPoint(e.getPoint());

                view.tftitulocancion.setText(view.tablacanciones.getValueAt(rowC, 0).toString());
                view.floatduracioncancion.setText(view.tablacanciones.getValueAt(rowC, 1).toString());
                view.inttrackcancion.setText(view.tablacanciones.getValueAt(rowC, 2).toString());
                view.floatpreciocancion.setText(view.tablacanciones.getValueAt(rowC, 3).toString());
                view.tfgenerocancion.setText(view.tablacanciones.getValueAt(rowC, 4).toString());
                view.cbcancionesdisco.setSelectedItem(view.tablacanciones.getValueAt(rowC, 5).toString());

                botonera.bteliminar.setEnabled(true);
                botonera.btmodificar.setEnabled(true);
            }
        });


        listenerCajasBuscar();
        ListenerBotones();
        limpiarTF();

        botonera.btnuevo.setEnabled(true);
        botonera.btguardar.setEnabled(false);
        botonera.btmodificar.setEnabled(false);
        botonera.btcancelar.setEnabled(false);

        edicionCampos(false);
        modeloTablas();
        listarTablas();
        combosRefrescar();
        limpiarTF();

    }


    private void ListenerBotones(){
        botonera.btnuevo.addActionListener(this);
        botonera.btguardar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.btconfi.addActionListener(this);
        botonera.btsalir.addActionListener(this);
    }

    private void listenerCajasBuscar(){
        view.tfbuscarGrupo.addKeyListener(this);
        view.tfbuscarDisco.addKeyListener(this);
        view.tfbuscarcancion.addKeyListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        int pestana = view.Panelprincipal.getSelectedIndex();

                if(e.getSource()==botonera.btnuevo){
                    limpiarTF();
                    botonera.btnuevo.setEnabled(false);
                    botonera.btguardar.setEnabled(true);
                    botonera.btmodificar.setEnabled(false);
                    botonera.btcancelar.setEnabled(true);



                    if (model.isAdmin()) {
                        botonera.btconfi.setEnabled(true);
                    }else{
                        botonera.btconfi.setEnabled(false);
                    }

                    modificar = false;
                    edicionCampos(true);


                }else if (e.getSource() == botonera.btguardar) {

                    switch (pestana){
                        case 0:
                            botonera.btnuevo.setEnabled(true);
                            botonera.btguardar.setEnabled(false);
                            botonera.btmodificar.setEnabled(true);
                            botonera.bteliminar.setEnabled(true);
                            botonera.btcancelar.setEnabled(true);

                            if (model.isAdmin()) {
                                botonera.btconfi.setEnabled(true);
                            }else{
                                botonera.btconfi.setEnabled(false);
                            }

                            Grupo grupo = new Grupo();
                            grupo.setNombre(view.tfnombregrupo.getText());
                            grupo.setGenero(view.tfgenerogrupo.getText());
                            grupo.setFechaInicio(view.datefiniciogrupo.getDate());
                            grupo.setFechaFin(view.dateffingrupo.getDate());
                            grupo.setNacionalidad(view.tfnacionalidadgrupo.getText());

                            if(modificar){
                                model.modifcarGrupo(grupo, idGrupo);
                            }else{
                                model.guardarGrupo(grupo);
                            }

                            edicionCampos(false);
                            listarTablas();
                            combosRefrescar();
                            limpiarTF();
                            break;
                        case 1:
                            edicionCampos(true);

                            Disco disco = new Disco();

                            disco.setTitulo(view.tftitulodisco.getText());
                            disco.setDuracion(Float.parseFloat(view.floatpreciodisco.getText()));
                            disco.setProductora(view.tfproductoradisco.getText());
                            disco.setNumCanciones(Integer.parseInt(view.intnumcancionesdisco.getText()));
                            disco.setPrecio(Float.parseFloat(view.floatpreciodisco.getText()));
                            disco.setProductora(view.tfproductoradisco.getText());
                            if(view.cbdiscosgrupo.getSelectedItem().toString() == view.cbdiscosgrupo.getItemAt(-1)){
                                JOptionPane.showMessageDialog(null,"Debes elegir un grupo , puedes crear uno anonimo para los discos que no sepas de que grupo son");
                                return;
                            }else{
                                for(Grupo grupos:listaGrupo){
                                    if(view.cbdiscosgrupo.getSelectedItem()==grupos.getNombre()){
                                        disco.setGrupo(grupos);
                                        break;
                                    }
                                }
                            }
                            if(modificar){
                                model.modificarDisco(disco,idDisco);
                            }else{
                                model.guardarDisco(disco);
                            }

                            botonera.btnuevo.setEnabled(true);
                            botonera.btguardar.setEnabled(false);
                            botonera.btmodificar.setEnabled(true);
                            botonera.bteliminar.setEnabled(true);
                            botonera.btcancelar.setEnabled(true);

                            botonera.btconfi.setEnabled(true);
                            if (!model.isAdmin()) {
                                botonera.btconfi.setEnabled(false);
                            }

                            edicionCampos(false);
                            listarTablas();
                            combosRefrescar();
                            limpiarTF();
                            break;
                        case 2:
                            edicionCampos(true);

                            Cancion cancion = new Cancion();

                            cancion.setTitulo(view.tftitulocancion.getText());
                            cancion.setDuracion(Float.parseFloat(view.floatduracioncancion.getText()));
                            cancion.setTrack(Integer.parseInt(view.inttrackcancion.getText()));
                            cancion.setPrecio(Float.parseFloat(view.floatpreciocancion.getText()));
                            cancion.setGenero(view.tfgenerocancion.getText());

                            if(view.cbcancionesdisco.getSelectedItem().toString() == view.cbcancionesdisco.getItemAt(-1)){
                                JOptionPane.showMessageDialog(null,"Debes elegir un DISCO , puedes crear uno anonimo para las canciones que no sepas de que grupo son");
                                return;
                            }else{
                                for(Disco discos : listaDisco){
                                    if(view.cbcancionesdisco.getSelectedItem() == discos.getTitulo()){
                                        cancion.setDisco(discos);
                                        break;
                                    }
                                }
                            }

                            if(modificar){
                                model.modificarCancion(cancion,idCancion);
                            }else{
                                model.guardarCancion(cancion);
                            }
                            botonera.btnuevo.setEnabled(true);
                            botonera.btguardar.setEnabled(false);
                            botonera.btmodificar.setEnabled(true);
                            botonera.bteliminar.setEnabled(true);
                            botonera.btcancelar.setEnabled(true);

                            botonera.btconfi.setEnabled(true);
                            if (!model.isAdmin()) {
                                botonera.btconfi.setEnabled(false);
                            }

                            edicionCampos(false);
                            listarTablas();
                            combosRefrescar();
                            limpiarTF();
                            break;
                    }

                }else if(e.getSource()==botonera.btmodificar){

                    if (model.isAdmin()) {
                        botonera.btconfi.setEnabled(true);
                    }else{
                        botonera.btconfi.setEnabled(false);
                    }

                    switch (pestana){
                        case 0:
                            edicionCampos(true);
                            modificar=true;
                            botonera.btguardar.setEnabled(true);
                            idGrupo = model.IdGrupo(view.tfnombregrupo.getText());

                            break;
                        case 1:
                            edicionCampos(true);
                            modificar=true;
                            botonera.btguardar.setEnabled(true);
                            idDisco = model.IdDisco(view.tftitulodisco.getText());
                            view.cbdiscosgrupo.setSelectedIndex(0);
                            break;
                        case 2:
                            edicionCampos(true);
                            modificar=true;
                            botonera.btguardar.setEnabled(true);
                            idCancion = model.IdCancion(view.tftitulocancion.getText());
                            view.cbcancionesdisco.setSelectedIndex(0);
                            break;
                    }
                }else if(e.getSource()==botonera.bteliminar){

                    if (model.isAdmin()) {
                        botonera.btconfi.setEnabled(true);
                    }else{
                        botonera.btconfi.setEnabled(false);
                    }

                    switch (pestana){
                        case 0:
                            idGrupo = model.IdGrupo(view.tfnombregrupo.getText());
                            model.eliminarGrupo(idGrupo);
                            listarTablas();
                            limpiarTF();
                            break;
                        case 1:
                            idDisco = model.IdDisco(view.tftitulodisco.getText());
                            model.eliminarDisco(idDisco);
                            listarTablas();
                            limpiarTF();
                            break;
                        case 2:
                            idCancion = model.IdCancion(view.tftitulocancion.getText());
                            model.eliminarCancion(idCancion);
                            listarTablas();
                            limpiarTF();
                            break;
                    }
                }else if(e.getSource()==botonera.btcancelar){

                    if (model.isAdmin()) {
                        botonera.btconfi.setEnabled(true);
                    }else{
                        botonera.btconfi.setEnabled(false);
                    }
                    switch (pestana){
                        case 0:
                            limpiarTF();
                            break;
                        case 1:
                            limpiarTF();
                            break;
                        case 2:
                            limpiarTF();
                            break;
                    }
                }else if(e.getSource()==botonera.btconfi){
                    configuracion =new Configuracion();
                    configuracion.setVisible(true);
                }else if(e.getSource()==botonera.btsalir){
                    model.desconectar();
                }
        }

    private void limpiarTF(){

        int pestana = view.Panelprincipal.getSelectedIndex();
        switch(pestana){
            case 0:
                view.tfnombregrupo.setText("");
                view.tfgenerogrupo.setText("");
                view.tfbuscarDisco.setText("");
                view.datefiniciogrupo.setDate(null);
                view.dateffingrupo.setDate(null);
                view.tfnacionalidadgrupo.setText("");
                break;
            case 1:
                view.tftitulodisco.setText("");
                view.tfbuscarDisco.setText("");
                view.floatduraciondisco.setText("");
                view.tfproductoradisco.setText("");
                view.intnumcancionesdisco.setText("");
                view.floatpreciodisco.setText("");

               // view.cbdiscosgrupo.setSelectedItem("Selecciona un grupo");

                break;
            case 2:
                view.tftitulocancion.setText("");
                view.floatduracioncancion.setText("");
                view.inttrackcancion.setText("");
                view.floatpreciocancion.setText("");
                view.tfgenerocancion.setText("");

               // view.cbcancionesdisco.setSelectedItem("Selecciona un disco");
                break;
            default:
                break;
        }
    }

    private void edicionCampos(boolean G){
        int pestana = view.Panelprincipal.getSelectedIndex();
        switch(pestana){
            case 0:
                view.tfnombregrupo.setEditable(G);
                view.tfgenerogrupo.setEditable(G);
                view.datefiniciogrupo.setEnabled(G);
                view.dateffingrupo.setEnabled(G);
                view.tfnacionalidadgrupo.setEditable(G);
                break;
            case 1:
                view.tftitulodisco.setEditable(G);
                view.floatduraciondisco.setEditable(G);
                view.tfproductoradisco.setEditable(G);
                view.intnumcancionesdisco.setEditable(G);
                view.floatpreciodisco.setEditable(G);

                view.cbdiscosgrupo.setEnabled(true);
                break;
            case 2:

                view.tftitulocancion.setEditable(G);
                view.floatduracioncancion.setEditable(G);
                view.inttrackcancion.setEditable(G);
                view.floatpreciocancion.setEditable(G);
                view.tfgenerocancion.setEditable(G);

                view.cbcancionesdisco.setEnabled(true);
                break;
            default:
                break;
        }
    }

    private void modeloTablas(){

                modelotablaGrupos = new DefaultTableModel();
                modelotablaGrupos.addColumn("Nombre");
                modelotablaGrupos.addColumn("Genero");
                modelotablaGrupos.addColumn("Fecha inicio");
                modelotablaGrupos.addColumn("Fecha fin");
                modelotablaGrupos.addColumn("Nacionalidad");
                view.tablagrupos.setModel(modelotablaGrupos);

                modelotablaDiscos = new DefaultTableModel();
                modelotablaDiscos.addColumn("Titulo");
                modelotablaDiscos.addColumn("Duracion");
                modelotablaDiscos.addColumn("Productora");
                modelotablaDiscos.addColumn("NēCanciones");
                modelotablaDiscos.addColumn("Precio");
                modelotablaDiscos.addColumn("Grupo");
                view.tabladiscos.setModel(modelotablaDiscos);

                modelotablaCanciones = new DefaultTableModel();
                modelotablaCanciones.addColumn("Titulo");
                modelotablaCanciones.addColumn("Duracion");
                modelotablaCanciones.addColumn("Track");
                modelotablaCanciones.addColumn("Precio");
                modelotablaCanciones.addColumn("Genero");
                modelotablaCanciones.addColumn("Disco");
                view.tablacanciones.setModel(modelotablaCanciones);

    }

    private void listarTablas(){
        int pestana = view.Panelprincipal.getSelectedIndex();

        switch(pestana){
            case 0:
                ResultSet resultado = model.listarGrupos();
                mostrarTablaGrupo(resultado);
                break;
            case 1:
                resultado = model.listarDiscos();
                mostrarTablaDiscos(resultado);
                break;
            case 2:
                resultado = model.listarCancion();
                mostrarTablaCanciones(resultado);
                break;
            default:
                break;
        }
    }

    private void mostrarTablaGrupo(ResultSet resultado){
            modelotablaGrupos.setNumRows(0);

        try {
            while(resultado.next()){
                String nombre = resultado.getString("nombre");
                String genero = resultado.getString("genero");
                Date finicio = resultado.getDate("fecha_inicio");
                Date ffin = resultado.getDate("fecha_fin");
                String nacionalidad = resultado.getString("nacionalidad");

                Object [] filaGrupo = new Object[]{nombre,genero,finicio,ffin,nacionalidad};
                modelotablaGrupos.addRow(filaGrupo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void mostrarTablaDiscos(ResultSet resultado){
        modelotablaDiscos.setNumRows(0);

        try {
            while(resultado.next()){
                String titulo = resultado.getString("titulo");
                float duracion = resultado.getFloat("duracion");
                String productora = resultado.getString("productora");
                int numcanciones = resultado.getInt("num_canciones");
                float precio = resultado.getFloat("precio");
                String nombreGrupo = resultado.getString("nombre_grupo");

                Object [] filaDisco = new Object[]{titulo,duracion,productora,numcanciones,precio,nombreGrupo};
                modelotablaDiscos.addRow(filaDisco);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void mostrarTablaCanciones(ResultSet resultado){
        modelotablaCanciones.setNumRows(0);

        try {
            while(resultado.next()){
                String titulo = resultado.getString("titulo");
                float duracion = resultado.getFloat("duracion");
                int track = resultado.getInt("track");
                float precio = resultado.getFloat("precio");
                String genero = resultado.getString("genero");
                String tituloCancion = resultado.getString("titulo_disco");

                Object [] filaCancion = new Object[]{titulo,duracion,track,precio,genero,tituloCancion};
                modelotablaCanciones.addRow(filaCancion);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void combosRefrescar(){
        view.cbdiscosgrupo.removeAllItems();
        view.cbcancionesdisco.removeAllItems();

        ResultSet resultadoGrupos = model.listarGrupos();
        try {
            while(resultadoGrupos.next()){
                Grupo grupo = new Grupo();
                grupo.setNombre(resultadoGrupos.getString("nombre"));
                grupo.setGenero(resultadoGrupos.getString("genero"));
                grupo.setFechaInicio(resultadoGrupos.getDate("fecha_inicio"));
                grupo.setFechaFin(resultadoGrupos.getDate("fecha_fin"));
                grupo.setNacionalidad(resultadoGrupos.getString("nacionalidad"));
                listaGrupo.add(grupo);
                view.cbdiscosgrupo.addItem(grupo.toString());

            }

            ResultSet resultadoDicos = model.listarDiscos();
            while(resultadoDicos.next()){
                Disco disco = new Disco();
                disco.setTitulo(resultadoDicos.getString("titulo"));
                disco.setDuracion(resultadoDicos.getFloat("duracion"));
                disco.setNumCanciones(resultadoDicos.getInt("num_canciones"));
                disco.setProductora(resultadoDicos.getString("productora"));
                disco.setPrecio(resultadoDicos.getInt("precio"));
                listaDisco.add(disco);
                view.cbcancionesdisco.addItem(disco.toString());

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int pestana = view.Panelprincipal.getSelectedIndex();
        switch(pestana){
            case 0:
                mostrarTablaGrupo(model.buscarGrupo(view.tfbuscarGrupo.getText()));
                break;
            case 1:
                mostrarTablaDiscos(model.buscarDisco(view.tfbuscarDisco.getText()));
                break;
            case 2:
                mostrarTablaCanciones(model.buscarCancion(view.tfbuscarcancion.getText()));
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
