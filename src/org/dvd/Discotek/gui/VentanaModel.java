package org.dvd.Discotek.gui;

import com.mysql.jdbc.*;
import org.dvd.Discotek.Interface.Configuracion;
import org.dvd.Discotek.Interface.Login;
import org.dvd.Discotek.base.Cancion;
import org.dvd.Discotek.base.Disco;
import org.dvd.Discotek.base.Grupo;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class VentanaModel {

    private String usuario,contrasena;
    private  Connection conexion;
    private int idDisco,idGrupo,idCancion;

    private boolean admin=false;

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public VentanaModel(){
        conectar();
    }


    private void conectar() {
        Properties properties = null;
        conexion = null;

        try {
            InputStream props = new FileInputStream("Configuracion.properties");
            properties = new Properties();
            properties.load(props);

            String driver = "com.mysql.jdbc.Driver";
            String tipoDB = "mysql";
            String servidor = properties.getProperty("servidor");
            String puerto = properties.getProperty("puerto");
            String db = properties.getProperty("base de datos");
            String usuario = properties.getProperty("usuario");
            String contrasena = properties.getProperty("contrasena");

            Class.forName(driver);

            conexion = DriverManager.getConnection("jdbc:"+tipoDB+"://"+servidor+":"+puerto+"/"+db,usuario,contrasena);

           // conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/discotek", "root", "mysql");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void desconectar(){

        try {

            JOptionPane.showMessageDialog(null, "Has sido desconectado ");
            conexion.close();

        } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,"Ha sido imposible desconectarse");
        }
    }
    public void Login(){

            Login login = new Login();
            login.setTitle("LOGIN");
            login.setVisible(true);


            usuario = login.getUsuario();
        if(usuario.equals("admin")){
            admin = true;

        }
            contrasena = login.getContrasena();

            String SQL = "SELECT * FROM usuario WHERE usuario=? AND contrasena=?";

            try {
                PreparedStatement sqlLogin = conexion.prepareStatement(SQL);
                sqlLogin.setString(1, usuario);
                sqlLogin.setString(2, contrasena);
                ResultSet resultado = sqlLogin.executeQuery();

                if (!resultado.next()) {
                    System.out.println("mal escrito");
                    JOptionPane.showMessageDialog(null, "Usuario/contraseņa mal escrito", "Error", JOptionPane.WARNING_MESSAGE);

                }
            } catch (SQLException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "La consulta esta mal ejecutada", "Error", JOptionPane.ERROR_MESSAGE);

            }


    }

    public ResultSet listarGrupos(){
        String sql = "SELECT * FROM grupo";
        ResultSet resultadoG = null;
        try {
            PreparedStatement sqlgrupo = conexion.prepareStatement(sql);
            resultadoG = sqlgrupo.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultadoG;
    }

    public ResultSet listarDiscos(){
        String sql = "SELECT * FROM disco";
        ResultSet resultadoD = null;
        try {
            PreparedStatement sqldisco = conexion.prepareStatement(sql);
            resultadoD = sqldisco.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultadoD;
    }
    public ResultSet listarCancion(){
        String sql = "SELECT * FROM cancion";
        ResultSet resultado= null;
        try {
            PreparedStatement sqlcancion = conexion.prepareStatement(sql);
            resultado = sqlcancion.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public void guardarGrupo(Grupo grupo){
        String sql = "INSERT INTO grupo (nombre,genero,fecha_inicio,fecha_fin,nacionalidad) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement sqlGrupo = conexion.prepareStatement(sql);

               sqlGrupo.setString(1,grupo.getNombre());
               sqlGrupo.setString(2,grupo.getGenero());
               sqlGrupo.setDate(3, new Date (grupo.getFechaInicio().getTime()));
               sqlGrupo.setDate(4, new Date (grupo.getFechaFin().getTime()));
               sqlGrupo.setString(5, grupo.getNacionalidad());

            sqlGrupo.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void guardarDisco(Disco disco){
        String sql = "INSERT INTO disco (titulo,duracion,productora,num_canciones,precio,nombre_grupo,id_grupo) VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement sqlDisco = conexion.prepareStatement(sql);

            sqlDisco.setString(1,disco.getTitulo());
            sqlDisco.setFloat(2, disco.getDuracion());
            sqlDisco.setString(3, disco.getProductora());
            sqlDisco.setInt(4, disco.getNumCanciones());
            sqlDisco.setFloat(5, disco.getPrecio());
            sqlDisco.setString(6, disco.getGrupo().getNombre());
            idGrupo = IdGrupo(disco.getGrupo().getNombre());
            sqlDisco.setInt(7, idGrupo);
            sqlDisco.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void guardarCancion(Cancion cancion){
        String sql = "INSERT INTO cancion (titulo ,duracion,track,precio,genero,titulo_disco,id_disco) VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement sqlCancion = conexion.prepareStatement(sql);

            sqlCancion.setString(1,cancion.getTitulo());
            sqlCancion.setFloat(2, cancion.getDuracion());
            sqlCancion.setInt(3, cancion.getTrack());
            sqlCancion.setFloat(4, cancion.getPrecio());
            sqlCancion.setString(5, cancion.getGenero());
            //sqlCancion.setString(6 , cancion.getGrupo().getNombre());
            sqlCancion.setString(6 , cancion.getDisco().getTitulo().toString());
            //idGrupo = IdGrupo(cancion.getGrupo().getNombre());
            //sqlCancion.setInt(8, idGrupo);
            idDisco = IdDisco(cancion.getDisco().getTitulo());
            sqlCancion.setInt(7, idDisco);

            sqlCancion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modifcarGrupo(Grupo grupo,int idG){
        String sql = "UPDATE grupo SET nombre=?,genero=?,fecha_inicio=?,fecha_fin=?,nacionalidad=? WHERE id=?";
        System.out.println("modificar grupo");
        try {
            PreparedStatement sqlModGrupo = conexion.prepareStatement(sql);

            sqlModGrupo.setString(1, grupo.getNombre());
            sqlModGrupo.setString(2, grupo.getGenero());
            sqlModGrupo.setDate(3, new Date(grupo.getFechaInicio().getTime()) );
            sqlModGrupo.setDate(4, new Date(grupo.getFechaFin().getTime()));
            sqlModGrupo.setString(5,grupo.getNacionalidad());
            sqlModGrupo.setInt(6, idG);

            sqlModGrupo.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("grupo modificado");
    }

    public void modificarDisco(Disco disco , int idD){
        String sql = "UPDATE disco SET titulo=?,duracion=?,productora=?,num_canciones=?,precio=?,nombre_grupo=?,id_grupo=? WHERE id=?";
        try {
            PreparedStatement sqlModDisco = conexion.prepareStatement(sql);

            sqlModDisco.setString(1, disco.getTitulo());
            sqlModDisco.setFloat(2, disco.getDuracion());
            sqlModDisco.setString(3, disco.getProductora());
            sqlModDisco.setInt(4, disco.getNumCanciones());
            sqlModDisco.setFloat(5, disco.getPrecio());
            sqlModDisco.setString(6, disco.getGrupo().getNombre());
            idGrupo =  IdGrupo(disco.getGrupo().getNombre());
            sqlModDisco.setInt(7 ,idGrupo);
            sqlModDisco.setInt(8,idD);

            sqlModDisco.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modificarCancion(Cancion cancion , int idC){
        String sql = "UPDATE cancion SET titulo=? ,duracion=?,track=?,precio=?,genero=?,titulo_disco=?,id_disco=? WHERE id=?";
        try {
            PreparedStatement sqlModCancion = conexion.prepareStatement(sql);

            sqlModCancion.setString(1,cancion.getTitulo());
            sqlModCancion.setFloat(2, cancion.getDuracion());
            sqlModCancion.setInt(3, cancion.getTrack());
            sqlModCancion.setFloat(4, cancion.getPrecio());
            sqlModCancion.setString(5, cancion.getGenero());
            sqlModCancion.setString(6, cancion.getDisco().getTitulo());
            idDisco = IdDisco(cancion.getDisco().getTitulo());
            sqlModCancion.setInt(7,idDisco);
            sqlModCancion.setInt(8,idC);
            sqlModCancion.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void eliminarGrupo(int id){
        String sql="DELETE  FROM grupo WHERE id=?";
        try {
            PreparedStatement sqlDG = conexion.prepareStatement(sql);
            sqlDG.setInt(1,id);
            sqlDG.executeUpdate();
            System.out.println("grupo eliminado");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void eliminarDisco(int id){
        String sql = "DELETE FROM disco WHERE id=?";
        try {
            PreparedStatement sqlDD = conexion.prepareStatement(sql);
            sqlDD.setInt(1,id);
            sqlDD.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void eliminarCancion(int id){
        String sql = "DELETE  FROM cancion WHERE id=?";
        try {
            PreparedStatement sqlDC = conexion.prepareStatement(sql);
            sqlDC.setInt(1,id);
            sqlDC.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int IdGrupo(String text){
        String sql = "SELECT id FROM grupo WHERE nombre=?";

        try {
            PreparedStatement sqlIdG = conexion.prepareStatement(sql);
            sqlIdG.setString(1, text);
            ResultSet resultado = sqlIdG.executeQuery();
            resultado.next();
            return resultado.getInt("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idGrupo;
    }

    public int IdDisco(String text){
        String sql = "SELECT id FROM disco WHERE titulo=?";
        try {
            PreparedStatement sqlIdD = conexion.prepareStatement(sql);
            sqlIdD.setString(1, text);
            ResultSet resultado = sqlIdD.executeQuery();
            resultado.next();
            return resultado.getInt("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idDisco;
    }

    public int IdCancion(String text){
        String sql="SELECT id FROM cancion WHERE titulo=?";
        try {
            PreparedStatement sqlIdC = conexion.prepareStatement(sql);
            sqlIdC.setString(1, text);
            ResultSet resultado = sqlIdC.executeQuery();
            resultado.next();
            return resultado.getInt("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idCancion;
    }

    public ResultSet buscarGrupo(String buscar){
        ResultSet resultado = null;
        String sql = "SELECT * FROM grupo WHERE nombre LIKE '"+buscar+"%' OR nacionalidad LIKE '"+buscar+"%'";
        try {
            PreparedStatement preparedStatement = conexion.prepareStatement(sql);
            resultado = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public ResultSet buscarDisco(String buscar){
        ResultSet resultado = null;
        String sql = "SELECT * FROM disco WHERE titulo LIKE '"+buscar+"%' OR productora LIKE '"+buscar+"%'";
        try {
            PreparedStatement preparedStatement = conexion.prepareStatement(sql);
            resultado = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public ResultSet buscarCancion(String buscar){
        ResultSet resultado = null;
        String sql = "SELECT * FROM cancion WHERE titulo LIKE '"+buscar+"%' OR genero LIKE '"+buscar+"%'";
        try {
            PreparedStatement preparedStatement = conexion.prepareStatement(sql);
            resultado = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
