package org.dvd.Discotek;


import org.dvd.Discotek.Interface.Ventana;
import org.dvd.Discotek.gui.VentanaController;
import org.dvd.Discotek.gui.VentanaModel;

/**
 * Created by k3ym4n on 14/02/2016.
 */
public class Principal {

    public static void main(String args[]){

        VentanaModel model = new VentanaModel();
        Ventana view = new Ventana();
        VentanaController controller = new VentanaController(view,model,view.getBotonera());
    }


}
