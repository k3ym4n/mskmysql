package org.dvd.Discotek.base;

import java.io.Serializable;

/**
 * Created by k3ym4n on 14/02/2016.
 */
//titulo ,duracion,track,precio,genero,idGrupo,idDisco
public class Cancion implements Serializable{

    private String titulo;
    private float duracion;
    private int track;
    private float precio;
    private String genero;


    private Disco disco;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Disco getDisco() {
        return disco;
    }

    public void setDisco(Disco disco) {
        this.disco = disco;
    }
}
